#!/usr/bin/python3

# import boto3
import argparse
import re
import os
import os.path
import sys
import json
import jinja2
import datetime
import email
import pprint
from email import policy
from email.message import EmailMessage
from requests.compat import urljoin
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad, Project

import stable_report_config as config

DEBUG = False
# DEBUG=False

if DEBUG:
    import requests_cache

    requests_cache.install_cache()

"""LKFT Stable Report

Environment variables accepted
------------------------------

    AWS_MODE            - to be set when deployed in AWS (any value will do)
    AWS_DYNAMODB_REGION - region to be used for AWS's dynamodb
    AWS_DYNAMODB_TABLEN - name of the table to be used in dynamodb
    LKFT_SCAN_DEPTH     - how many already scanned builds to check when
                          looking for skipped ones
    LKFT_PROJECT_LIST   - list of projects (and, possibly, builds) to process;
                          if not specified - pattern matching is used
                          against the list of projects in squad and builds
                          are checked against a dynamodb table
"""

# url length limit
MAX_URL_LENGTH = 2000

# set to any value if running in an AWS environment
ENV_AWS_MODE = "AWS_MODE"

# name of the environment variable that has email region name
ENV_AWS_EMAIL_REGION = "us-east-1"

# current node index (1-based) that is set by gitlab for parallel jobs
ENV_PARALLEL_JOB_ID = "CI_NODE_INDEX"

# total number of parallel jobs that is set by gitlab for parallel jobs
ENV_PARALLEL_JOB_TOTAL = "CI_NODE_TOTAL"

# url of the job details
ENV_JOB_DETAILS_URL = "CI_JOB_URL"

# directory where all the reporting jobs are stored
DIRECTORY_JOBS = "jobs"

# directory where all the report files are stored
DIRECTORY_REPORTS = "reports"

# directory where all the summary files are stored
DIRECTORY_SUMMARIES = "summaries"

# group:project:build,build,build;group:project;group:project:build,group:project:build,build

# list of groups, projects (and exact builds) to process
# the format is:
#   <job-definitions> ::= <job-definition> | ";" <job-definitions>
#   <job-definition>  ::= <first-triplet> "," <triplet> "," <triplet>
#                       | <first-triplet> "," <triplet>
#                       | <first-triplet> ;
#   <first-triplet>   ::= <group> ":" <project> ":" <build>
#                       | <group> ":" <project>
#                       | <group> ;
#   <triplet>         ::= <group> ":" <project> ":" <build>
#                       | <project> ":" <build>
#                       | <build> ;
#   <group>           ::= <name> ;
#   <project>         ::= <name> ;
#   <build>           ::= <name> ;
#
# if not set - all the projects matching config.REGEXP_STABLE_PROJECT will be processed
# and builds that have not yet been reported will be processed, automatically finding the suitable previous and base builds
ENV_JOBS = "LKFT_JOBS"

# regexp to parse build's name
REGEXP_STABLE_BUILD = "^v(?P<major>\d+)\.(?P<minor>\d+)(?:\.(?P<patch>\d+))?(?:-(?P<count>\d+)-g(?P<hash>[\da-f]+))?$"

# parse report file name
REGEXP_REPORT_FILE_NAME = "^(?P<group>.*?)--(?P<project>.*?)--(?P<build>.*)\.[^.]*$"


# parse suite url to get suite id
# https://qa-reports.linaro.org/api/suites/66446/
REGEXP_SUITE_URL = "^.*?://[^/]+/api/suites/(?P<id>\d+).*$"

# parse suite url to get suite id
# https://qa-reports.linaro.org/api/environments/74/
REGEXP_ENVIRONMENT_URL = "^.*?://[^/]+/api/environments/(?P<id>\d+).*$"

# deployed to AWS - enable all the bells and whistles
if os.environ.get(ENV_AWS_MODE):

    import boto3
    from botocore.exceptions import ClientError

    print("Operation mode: deployed to AWS")

    def send_email(email):
        from_address = email["From"].strip()
        to_addresses = (
            [addressee.strip() for addressee in email["To"].split(",")]
            if email["To"]
            else None
        )
        cc_addresses = (
            [addressee.strip() for addressee in email["Cc"].split(",")]
            if email["Cc"]
            else None
        )

        addresses = {"ToAddresses": to_addresses}
        if cc_addresses:
            addresses["CcAddresses"] = cc_addresses

        subject = email["Subject"]
        body = email.get_body()

        response = None
        try:
            # Create a new SES resource and specify a region.
            client = boto3.client("ses", region_name=ENV_AWS_EMAIL_REGION)

            response = client.send_email(
                Destination=addressee,
                Message={
                    "Body": {"Text": {"Charset": "UTF-8", "Data": body}},
                    "Subject": {"Charset": "UTF-8", "Data": subject},
                },
                Source=from_address,
            )
        except ClientError as e:
            print(e.response["Error"]["Message"])
            return False

        return True


# not deployed to AWS - limited functionality
else:  # not os.environ.get(ENV_AWS_MODE)

    print("Operation mode: standalone")

    def send_email(email):
        return False


def get_build_version_parts(build_version):
    match = re.match(REGEXP_STABLE_BUILD, build_version)
    if not match:
        return None

    result = {}

    value = match.group("major")
    if value:
        result["major"] = value

    value = match.group("minor")
    if value:
        result["minor"] = value

    value = match.group("patch")
    if value:
        result["patch"] = value

    value = match.group("count")
    if value:
        result["count"] = value

    value = match.group("hash")
    if value:
        result["hash"] = value

    return result


def complement_triplets(squad, job, include_unfinished=False):
    """Make sure the triplets are fully qualified (all 3 values are real)"""
    errors = []

    build_triplet = job.get("build")
    previous_triplet = job.get("previous")
    base_triplet = job.get("base")

    build_iters = {}

    group = build_triplet.get("group")
    project = build_triplet.get("project")
    build = build_triplet.get("build")
    if not build or build == "?":
        key = "{0}:{1}".format(group.slug, project.slug)
        build_iter = build_iters.get(key)
        if not build_iter:
            project.build_collection = BuildCollection(project, include_unfinished)
            build_iter = build_iters[key] = project.build_collection.__iter__()
        build = build_iter.next()
        if not build:
            errors.append("Project {0} has no builds".format(project.slug))
            return errors

        build_triplet["build"] = build

    if previous_triplet:
        group = previous_triplet.get("group")
        project = previous_triplet.get("project")
        build = previous_triplet.get("build")
        if not build or build == "?":
            key = "{0}:{1}".format(group.slug, project.slug)
            build_iter = build_iters.get(key)
            if not build_iter:
                project.build_collection = BuildCollection(project, include_unfinished)
                build_iter = build_iters[key] = project.build_collection.__iter__()
            build = build_iter.peek()
            if not build:
                del job["previous"]
            else:
                previous_triplet["build"] = build

    if base_triplet:
        group = base_triplet.get("group")
        project = base_triplet.get("project")
        build = base_triplet.get("build")
        if not build or build == "?":
            key = "{0}:{1}".format(group.slug, project.slug)
            build_iter = build_iters.get(key)
            if not build_iter:
                project.build_collection = BuildCollection(project, include_unfinished)
                build_iter = build_iters[key] = project.build_collection.__iter__()

            last_parts = None
            for build in build_iter:
                parts = get_build_version_parts(build.version)
                if not parts:
                    continue
                if not parts.get("count"):
                    base_triplet["build"] = build
                    break
                if last_parts and (
                    parts.get("major") != last_parts.get("major")
                    or parts.get("minor") != last_parts.get("minor")
                    or parts.get("patch") != last_parts.get("patch")
                ):
                    base_triplet["build"] = build
                    break
                last_parts = parts

            if not build or build == "?":
                del job["base"]

    return errors


def list_builds(squad, args):
    """
    List all the builds for each of the projects that match the regexp.
    """

    success, jobs = parse_job_definitions(squad, args, True)
    if not success:
        return 1

    if not jobs:
        groups = squad.groups(count=-1)
        if not groups:
            print("There are no groups")
        else:
            print("Groups:")
            for group in groups.values():
                print(
                    "- {0}{1}".format(
                        group.slug,
                        (" ({0})".format(group.name) if group.name else "")
                    )
                )
        return 0

    for job_id, job in enumerate(jobs):
        print("Job {0}:".format(job_id))
        build_triplet = job.get("build")
        previous_triplet = job.get("previous")
        base_triplet = job.get("base")

        num_triplets = (
            (1 if build_triplet else 0)
            + (1 if previous_triplet else 0)
            + (1 if base_triplet else 0)
        )

        if num_triplets > 1:  # full job specification
            complement_triplets(squad, job, args.all)

            for triplet_type in ("build", "previous", "base"):
                triplet = job.get(triplet_type)
                if not triplet:
                    continue
                group = triplet.get("group")
                project = triplet.get("project")
                build = triplet.get("build")
                print(
                    "- {0}: {1}:{2}{3}".format(
                        triplet_type,
                        group.slug
                            if group and group != "?"
                            else ("?" if group else ""),
                        project.slug
                            if project and project != "?"
                            else ("?" if group else ""),
                        (":" + build.version)
                            if build and build != "?"
                            else (":?" if build else ""),
                    )
                )

        else:
            for triplet_type in ("build", "previous", "base"):
                triplet = job.get(triplet_type)
                if not triplet:
                    continue
                group = triplet.get("group")
                project = triplet.get("project")
                build = triplet.get("build")
                print(
                    "- {0}: {1}:{2}{3}".format(
                        triplet_type,
                        group.slug
                        if group and group != "?"
                        else ("?" if group else ""),
                        project.slug
                        if project and project != "?"
                        else ("?" if group else ""),
                        (":" + build.version)
                        if build and build != "?"
                        else (":?" if build else ""),
                    )
                )
                if project == "?":
                    projects = group.projects(count=-1)
                    for project in projects.values():
                        print("    {0}:{1}".format(group.slug, project.slug))
                elif build == "?":
                    project.build_collection = BuildCollection(project, args.all)
                    if not len(project.build_collection):
                        print("    none")
                    else:
                        for build in project.build_collection:
                            print(
                                "    {0}:{1}:{2}".format(
                                    group.slug, project.slug, build.version
                                )
                            )
                else:
                    print("    {0}:{1}".format(group.slug, project.slug))

    return 0


def unique_from_sorted(a_list):
    result = []
    for item in a_list:
        if result and result[-1] == item:
            continue
        result.append(item)

    return result


def list_suites(squad, args):
    """
    List all the suites.
    """

    suites = squad.suites(count=-1)
    suites = unique_from_sorted(sorted([suite.slug for suite in suites.values()]))
    print("Suites ({0}):".format(len(suites)))
    for suite in suites:
        if suite.strip() == "":
            continue
        print(" - {0}".format(suite))

    return 0


def list_environments(squad, args):
    """
    List all the environments.
    """

    environments = squad.environments(count=-1)
    environments = unique_from_sorted(sorted([env.slug for env in environments.values()]))
    print("Environments ({0}):".format(len(environments)))
    for environment in environments:
        if environment.strip() == "":
            continue
        print(" - {0}".format(environment))

    return 0


class BuildCollection(object):
    """
    Build collection that allows working with a set of builds.
    """

    class Iterator(object):
        """
        Build collection iterator, that allows peeking at non-current items.
        """

        def __init__(self, collection, offset=None):
            self.collection = collection
            self.pointer = (
                (offset if offset != None else 0) if len(collection.builds) else None
            )

        def __iter__(self):
            return self

        def __next__(self):
            item = self.next()
            if item == None:
                raise StopIteration()

            return item

        def __len__(self):
            if self.pointer == None:
                return 0

            return len(self.collection.builds) - self.pointer

        def clone(self):
            return BuildCollection.Iterator(self.collection, self.pointer)

        def next(self):
            if self.pointer == None:
                return None

            item = self.collection.builds[self.pointer]
            self.pointer += 1
            if self.pointer >= len(self.collection.builds):
                self.pointer = None
            return item

        def peek(self, offset=0):
            if self.pointer == None:
                return None

            peek_at = self.pointer + offset
            if peek_at < 0 or peek_at >= len(self.collection.builds):
                return None

            return self.collection.builds[peek_at]

        def reset(self):
            self.pointer = None if not len(self.collection.builds) else 0

        def collection(self):
            return self.collection

    # end of class Iterator

    def __init__(self, project, include_unfinished=False):
        self.project = project
        builds = project.builds(count=-1, status__finished=not include_unfinished)
        self.by_version = {}
        self.builds = []
        if builds:
            for build_id, build in builds.items():
                self.by_version[build.version] = build
                self.builds.append(build)

    def __iter__(self):
        return self.Iterator(self)

    def values(self):
        return self.Iterator(self)

    def build(self, version_name):
        return self.by_version.get(version_name)

    def __len__(self):
        return len(self.builds)


# end of class BuildCollection


def get_report_setup(squad, args, project, build_to_report, build_iter):
    """
    Build a dictionary with setup for a single report.

    First - get the previous (time wise) build as 'previous' build
    Then - find the last non-patched build as 'base' build
           if not found - use the last patched build of previous version
    """

    builds = build_iter.collection
    iter = build_iter.clone()

    match_to_build = re.match(REGEXP_STABLE_BUILD, build_to_report.version)
    if not match_to_build:
        print(
            "Build {0} of project {1} did not match the regexp: {2}".format(
                build.version, project.slug, REGEXP_STABLE_BUILD
            )
        )
        return None

    prev_build = build_iter.peek()

    setup = {
        "group": group.slug,
        "project": project.slug,
        "build": build_to_report.version,
        "prev_build": prev_build.version if prev_build else None,
        "base_build": None,
    }

    # should the base build be the same version (with no patches on top)
    same_version = match_to_build.group("count")
    version_to_search_in = None

    # find base build if possible
    for build in build_iter.clone():
        match = re.match(REGEXP_STABLE_BUILD, build.version)

        # if build_to_report is a no-patches-on-top build
        # search for the previous version's build with no patches on top
        # or the last one with patches on top from two versions back

        if same_version:
            # search within the same version
            if (
                match.group("major") == match_to_build.group("major")
                and match.group("minor") == match_to_build.group("minor")
                and match.group("patch") == match_to_build.group("patch")
            ):

                # found a no-patches-on-top version
                if not match.group("count"):
                    setup["base_build"] = build.version
                    return setup

            # previous version - pick the lastest build
            else:
                setup["base_build"] = build.version
                return setup

        # if build_to_report has patches on top
        # search for the current version's build with no patches on top
        # or the last one with patches on top from the previous version

        else:
            # search within the previous version
            if (
                match.group("major") == match_to_build.group("major")
                and match.group("minor") == match_to_build.group("minor")
                and match.group("patch") == match_to_build.group("patch")
            ):
                continue

            if not version_to_search_in:
                version_to_search_in = (
                    match.group("major"),
                    match.group("minor"),
                    match.group("patch"),
                )

            if (
                match.group("major") == version_to_search_in[0]
                and match.group("minor") == version_to_search_in[1]
                and match.group("patch") == version_to_search_in[2]
            ):
                if not match.group("count"):
                    setup["base_build"] = build.version
                    return setup
                else:
                    continue

            setup["base_build"] = build.version
            return setup

    return setup


def parse_jobs(squad, args):
    jobs = []

    sources = (
        ("args", args.jobs),
        (
            ENV_JOBS,
            [item.strip() for item in os.environ.get(ENV_JOBS).split(";")]
            if os.environ.get(ENV_JOBS)
            else None,
        ),
    )

    for source_name, triplets in sources:
        if not source.strip():
            continue

        if len(triplets) == 1 and triplets[0] == "":
            continue

        group_slug = None
        group = None
        project_slug = None
        project = None
        build_version = None
        build = None

        for triplet in triplets:
            fq_builds = [item.strip() for item in triplet.split(",")]
            for fq_build in fq_builds:
                parts = [item.strip() for item in fq_build.split(":")]
                # the only item means build
                if len(parts) == 1:
                    if parts[0]:
                        build_version = parts[0]
                        build = None
                # two items mean project and build
                if len(parts) == 2:
                    if parts[0]:
                        project_slug = parts[0]
                        project = None
                    if parts[1]:
                        build_version = parts[1]
                        build = None
                # all three are present
                if len(parts) >= 3:
                    if parts[0]:
                        group_slug = parts[0]
                        group = None
                    if parts[1]:
                        project_slug = parts[1]
                        project = None
                    if parts[2]:
                        build_version = parts[2]
                        build = None

                if not group_slug:
                    print("No group specified.")
                    return []

                if not project_slug:
                    print("No project specified. Aborting.")
                    return []

                if group_slug and not group:
                    group = squad.group(group_slug)
                    if not group:
                        print("Unknown group: {0}".format(group_slug))
                        return []

                if project_slug and not project:
                    project = get_project(squad, group, project_slug)
                    if not project:
                        print("Unknown project: {0}".format(project_slug))
                        return []

                if build_version and not build:
                    build = project.build(build_version)
                    if not build:
                        print("Unknown build: {0}".format(build_version))
                        return []

                # TODO: check if something is missing
                # TODO: add to the list of jobs

    return jobs


def job_to_json(job):
    json = {}

    for triplet_type in ("build", "previous", "base"):
        triplet = job.get(triplet_type)
        if not triplet:
            continue

        json_triplet = {}
        group = triplet.get("group")
        if group:
            json_triplet["group"] = group.slug

        project = triplet.get("project")
        if project:
            json_triplet["project"] = project.slug

        build = triplet.get("build")
        if build:
            json_triplet["build"] = build.version

        json[triplet_type] = json_triplet

    return json


def scan_builds(squad, args):
    """
    Prepare jobs for the report generation.
    """

    try:
        os.mkdir(DIRECTORY_JOBS)
    except FileExistsError:
        pass
    except:
        print(
            "Exception creating the job directory '{0}': {1}".format(
                DIRECTORY_JOBS, sys.exc_info()[0]
            )
        )
        return 2

    try:
        os.mkdir(DIRECTORY_SUMMARIES)
    except FileExistsError:
        pass
    except:
        print(
            "Exception creating the summaries directory '{0}': {1}".format(
                DIRECTORY_SUMMARIES, sys.exc_info()[0]
            )
        )
        return 2

    try:
        os.mkdir(DIRECTORY_REPORTS)
    except FileExistsError:
        pass
    except:
        print(
            "Exception creating the report directory '{0}': {1}".format(
                DIRECTORY_REPORTS, sys.exc_info()[0]
            )
        )
        return 2

    success, jobs = parse_job_definitions(squad, args)
    if not jobs:
        print("No jobs specified either as args or in {0}. Aborting.".format(ENV_JOBS))
        return 3

    print("Preparing report jobs")

    job_file_counter = 1

    for job_id, job in enumerate(jobs):
        complement_triplets(squad, job, args.all)

        json_job = job_to_json(job)
        print("Job {0}:".format(job_id))
        print(json.dumps(json_job, indent=2))

        job_file_name = "{0}/{1:02d}.{2}.txt".format(
            DIRECTORY_JOBS, job_file_counter, job["build"]["project"].slug
        )
        job_file_counter += 1
        with open(job_file_name, "w") as fp:
            json.dump(json_job, fp, indent=2)

    return 0


def generate_reports(squad, args):
    """
    Generate reports as requested in "jobs/*.txt" files.


    TODO: if total number of workers is smaller than the number of jobs,
          each worker does their "column" of jobs (% wise)
    """

    print("Generating reports")

    try:
        worker_index = int(os.environ.get(ENV_PARALLEL_JOB_ID, "1"))
        try:
            worker_count = int(os.environ.get(ENV_PARALLEL_JOB_TOTAL, "1"))
        except ValueError:
            worker_index = 1
            worker_count = 1
            print(
                "Envvar {0} has an invalid value: {1}. Defaulting to single worker.".format(
                    ENV_PARALLEL_JOB_TOTAL, os.environ.get(ENV_PARALLEL_JOB_TOTAL, "1")
                )
            )
    except ValueError:
        worker_index = 1
        worker_count = 1
        print(
            "Envvar {0} has an invalid value: {1}. Defaulting to single worker.".format(
                ENV_PARALLEL_JOB_ID, os.environ.get(ENV_PARALLEL_JOB_ID, "1")
            )
        )

    try:
        os.mkdir(DIRECTORY_REPORTS)
    except FileExistsError:
        pass
    except:
        print(
            "({0}) Exception creating the report directory '{1}': {2}".format(
                job_index, DIRECTORY_REPORTS, sys.exc_info()[0]
            )
        )

    # get the list of job files
    try:
        job_files = os.listdir(DIRECTORY_JOBS)
    except FileNotFoundError as e:
        print(
            "{0}Job directory '{1}' not found".format(
                "({0}) ".format(worker_index) if worker_index != None else "",
                DIRECTORY_JOBS,
            )
        )
        return 1

    if worker_index > len(job_files):
        print(
            "{0}No work for this job (not enough work for all, consider decreasing parallelism.".format(
                "({0}) ".format(worker_index) if worker_index != None else ""
            )
        )
        return 1

    if not job_files:
        print(
            "{0}No reporting jobs found".format(
                "({0}) ".format(worker_index) if worker_index != None else ""
            )
        )
        return 1

    job_files.sort()
    job_index = int(worker_index)

    # run all jobs for the worker
    while job_index <= len(job_files):
        job_file = os.path.join(DIRECTORY_JOBS, job_files[job_index - 1])
        process_job_file(squad, args, job_file, job_index)
        job_index += worker_count

    return 0


def process_job_file(squad, args, job_file_name, job_index=None):
    """
    Generate reports for a single job file (i.e. single project)
    """

    try:
        with open(job_file_name, "r") as fd:
            json_text = fd.read()
    except OSError as e:
        print(
            "{0}Unable to read job file '{1}': {2}".format(
                "({0}) ".format(job_index) if job_index != None else "",
                job_file_name,
                e.strerror,
            )
        )
        return

    try:
        setup = json.loads(json_text)
    except JSONDecodeError as e:
        print(
            "{0}Unable to parse job file '{1}': {2}".format(
                "({0}) ".format(job_index) if job_index != None else "",
                job_file_name,
                e.msg,
            )
        )
        return

    print(
        "{0}Processing job {1}:\n{2}".format(
            "({0}) ".format(job_index) if job_index != None else "",
            job_file_name,
            json.dumps(setup, indent=2),
        )
    )

    build_report(squad, args, setup)


def increment_build_counter(stats, counter_type):
    if stats["build"].get(counter_type) == None:
        stats["build"][counter_type] = 0
    stats["build"][counter_type] += 1


def increment_environment_counter(stats, environment, counter_type):
    env_record = stats["environments"].get(environment)
    if not env_record:
        env_record = stats["environments"][environment] = {
            "suites": {},
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
        }
    if env_record.get(counter_type) == None:
        env_record[counter_type] = 0
    env_record[counter_type] += 1


def increment_suite_counter(stats, environment, suite, counter_type):
    # suite counter
    suite_record = stats["suites"].get(suite)
    if not suite_record:
        suite_record = stats["suites"][suite] = {
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
        }
    if suite_record.get(counter_type) == None:
        suite_record[counter_type] = 0
    suite_record[counter_type] += 1
    # suite-in-environment counter
    env_record = stats["environments"].get(environment)
    if not env_record:
        env_record = stats["environments"][environment] = {
            "suites": {},
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
        }
    suite_record = env_record["suites"].get(suite)
    if not suite_record:
        suite_record = env_record["suites"][suite] = {
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
        }
    if suite_record.get(counter_type) == None:
        suite_record[counter_type] = 0


# in memory cache of suite names retrieved by suite id from suite urls
g_suite_name_by_url = {}


def suite_name_from_url(project, url):
    global g_suite_name_by_url

    lookup_result = g_suite_name_by_url.get(url)
    if lookup_result:
        return lookup_result

    match = re.match(REGEXP_SUITE_URL, url)
    if not match:
        suite_name = url
    else:
        suite = project.suites(id=match.group("id"))
        if suite:
            suite = list(suite.values())[0]
            if suite:
                suite_name = suite.slug
            else:
                suite_name = "suite-{0}".format(match.group("id"))
        else:
            suite_name = "suite-{0}".format(match.group("id"))

    g_suite_name_by_url[url] = suite_name

    return suite_name


# in memory cache of environment names retrieved by environment id from environment urls
g_environment_name_by_url = {}


def environment_name_from_url(squad, url):
    global g_environment_name_by_url

    lookup_result = g_environment_name_by_url.get(url)
    if lookup_result:
        return lookup_result

    match = re.match(REGEXP_ENVIRONMENT_URL, url)
    if not match:
        environment_name = url
    else:
        environment = squad.environments(id=match.group("id"))
        if environment:
            environment = list(environment.values())[0]
            if environment:
                environment_name = environment.slug
            else:
                environment_name = "environment-{0}".format(match.group("id"))
        else:
            environment_name = "environment-{0}".format(match.group("id"))

    g_environment_name_by_url[url] = environment_name

    return environment_name


def get_build_stats(squad, build_triplet, environments, suites):
    group = build_triplet["group"]
    project = build_triplet["project"]
    build = build_triplet["build"]

    build_stats = {
        "build": {"pass": 0, "fail": 0, "skip": 0, "xfail": 0},
        "environments": {},
        "suites": {},
        "failures": [],
        "skips": [],
    }

    testruns = build.testruns(count=-1)

    for testrun_id, testrun in testruns.items():
        tests = testrun.tests()

        for test_id, test in tests.items():
            increment_build_counter(build_stats, test.status)

            env_name = environment_name_from_url(squad, testrun.environment)
            if environments and env_name not in environments:
                continue

            suite_name = suite_name_from_url(project, test.suite)
            if suites and suite_name not in suites:
                continue

            increment_environment_counter(
                build_stats, env_name, test.status
            )
            increment_suite_counter(
                build_stats, env_name, test.suite, test.status
            )

            if test.status == "fail":
                build_stats["failures"].append(
                    (env_name, suite_name, test.name)
                )
                build_stats["build"][test.status] += 1

            elif test.status == "skip":
                build_stats["skips"].append(
                    (env_name, suite_name, test.name)
                )
                build_stats["build"][test.status] += 1

            elif test.status == "pass":
                build_stats["build"][test.status] += 1

            elif test.status == "xfail":
                build_stats["build"][test.status] += 1

    sorted_suites = []
    for suite_url, stats in build_stats["suites"].items():
        suite = suite_name_from_url(project, suite_url)
        sorted_suites.append((suite, stats))

    sorted_suites = sorted(
        ((suite, stats) for suite, stats in sorted_suites), key=lambda item: item[0]
    )

    build_stats["suites"] = sorted_suites

    # sort all suites under environments
    for environment, stats in build_stats["environments"].items():
        suites = stats["suites"]
        sorted_suites = sorted(
            (
                (
                    suite_name_from_url(project, url),
                    stats,
                )
                for url, stats in suites.items()
            ),
            key=lambda item: item[0],
        )
        stats["suites"] = sorted_suites

    sorted_environments = sorted(
        ((name, stats) for name, stats in build_stats["environments"].items()),
        key=lambda item: item[0],
    )
    build_stats["environments"] = sorted_environments

    return build_stats


def get_triplet_from_setup(squad, setup):
    group = squad.group(setup["group"])
    if not group:
        return None

    project = group.project(setup["project"])
    if not project:
        return None

    build = project.build(setup["build"])
    if not build:
        return None

    return {"group": group, "project": project, "build": build}


def build_report(squad, args, report_setup):
    environments = args.environments
    suites = args.suites

    build = report_setup.get("build")
    previous = report_setup.get("previous")
    base = report_setup.get("base")

    build_triplet = get_triplet_from_setup(squad, build) if build else None
    previous_triplet = get_triplet_from_setup(squad, previous) if previous else None
    base_triplet = get_triplet_from_setup(squad, base) if base else None

    print(
        "Generating report for build {0} of {1} ...".format(
            build_triplet["build"].version, build_triplet["project"].slug
        )
    )

    if environments:
        print("Environments to report for: {0}".format(", ".join(environments)))

    if suites:
        print("Suites to report for: {0}".format(", ".join(suites)))

    if previous:
        compared_to_previous = compare_builds(
            previous_triplet["build"], build_triplet["build"], suites, environments
        )
    else:
        compared_to_previous = None

    if base_triplet and base_triplet["build"] != build_triplet["build"]:
        compared_to_base = compare_builds(
            base_triplet["build"], build_triplet["build"], suites, environments
        )

    else:
        base_triplet = None
        compared_to_base = None

    build_details_url = urljoin(
        SquadApi.url,
        "%s/%s/build/%s"
        % (
            build_triplet["group"].slug,
            build_triplet["project"].slug,
            build_triplet["build"].version,
        ),
    )

    template_env = jinja2.Environment(
        extensions=["jinja2.ext.loopcontrols"],
        loader=jinja2.FileSystemLoader("templates"),
        trim_blocks=True,
        lstrip_blocks=True,
    )

    build_stats = get_build_stats(squad, build_triplet, environments, suites)

    report = template_env.get_template("stable-build-report.jinja").render(
        build=build_triplet,
        previous=previous_triplet,
        base=base_triplet,
        compared_to_previous=compared_to_previous,
        compared_to_base=compared_to_base,
        build_details_url=build_details_url,
        build_stats=build_stats,
        environments=environments,
        suites=suites,
    )

    msg = email.message_from_bytes(bytearray(report, "utf-8"))

    # remove and readd after other headers to get it as a last header
    subject = msg["Subject"]
    del msg["Subject"]

    setup = {"From": "LKFT <lkft@linaro.org>", "To": "LKFT <lkft@linaro.org>"}
    for regexp, a_setup in config.report_email_by_project:
        match = re.match(regexp, build_triplet["project"].slug)
        if match:
            setup.update(a_setup)
            break

    msg["From"] = setup.get("From", config.default_report_from)

    values = setup.get("To", config.default_report_to)
    msg["To"] = ", ".join(values)

    values = setup.get("Cc", config.default_report_cc)
    if values and len(values):
        msg["Cc"] = ", ".join(values)

    msg["Subject"] = subject

    file_name = "{0}/{1}--{2}--{3}.eml.txt".format(
        DIRECTORY_REPORTS, build_triplet["group"].slug, build_triplet["project"].slug,
        build_triplet["build"].version
    )

    with open(
        file_name,
        "w",
    ) as f:
        f.write(msg.as_string(policy=msg.policy.clone(max_line_length=0)))
    print("... wrote the report into {0}".format(file_name))

    summary = template_env.get_template("stable-build-summary.jinja").render(
        build=build_triplet,
        previous_build=previous_triplet,
        base_build=base_triplet,
        compared_to_previous=compared_to_previous,
        compared_to_base=compared_to_base,
        build_details_url=build_details_url,
        build_stats=build_stats,
        details_url=os.environ.get(ENV_JOB_DETAILS_URL),
    )
    summary_file_name = "{0}/{1}.txt".format(
        DIRECTORY_SUMMARIES, build_triplet["project"].slug
    )
    print("Summary in {0}:\n{1}\n".format(summary_file_name, summary))
    with open(summary_file_name, "a") as fp:
        fp.write(summary)

    return 0


def send_notifications(squad, args):
    try:
        summary_files = os.listdir(DIRECTORY_SUMMARIES)
    except FileNotFoundError as e:
        print("Summary directory '{1}' not found".format(DIRECTORY_SUMMARIES))
        return 1

    if not summary_files:
        print("No summaries found")
        return 1

    summary_files.sort()

    body = ""
    for summary_file in summary_files:
        with open(os.path.join(DIRECTORY_SUMMARIES, summary_file), "r") as f:
            body += f.read()
    body += "\n-- \nLKFT"

    msg = EmailMessage(policy=policy.default.clone(max_line_length=MAX_URL_LENGTH))
    msg.set_content(body)

    msg["From"] = config.notification_email.get(
        "From", config.default_notification_from
    )

    values = config.notification_email.get("To")
    if len(values):
        msg["To"] = ", ".join(values)
    else:
        msg["To"] = config.default_notification_to

    values = config.notification_email.get("Cc")
    if values and len(values):
        msg["Cc"] = ", ".join(values)
    elif config.default_notification_cc and len(config.default_notification_cc):
        msg["Cc"] = ", ".join(config.default_notification_cc)

    msg["Subject"] = config.notification_email.get(
        "Subject", config.default_notification_subject
    )

    with open("{0}/summary.eml.txt".format(DIRECTORY_SUMMARIES), "w") as f:
        f.write(msg.as_string())

    send_email(email)

    return 0


def send_reports(squad, args):
    try:
        report_files = os.listdir(DIRECTORY_REPORTS)
    except FileNotFoundError as e:
        print("Report directory '{1}' not found".format(DIRECTORY_REPORTS))
        return 1

    if not report_files:
        print("No summaries found")
        return 1

    report_files.sort()

    for report_file in report_files:
        match = re.match(REGEXP_REPORT_FILE_NAME, report_file)
        if not match:
            print("Skipped a report file with an invalid name: {0}".format(report_file))
            continue

        group = squad.group(match.group("group"))
        if not group:
            print("Unknown group in report file: {0}".format(report_file))
            continue

        project = group.project(match.group("project"))
        if not project:
            print("Unknown project in report file: {0}".format(report_file))
            continue

        build = project.build(match.group("build"))
        if not build:
            print("Unknown build in report file: {0}".format(report_file))
            continue

        with open(os.path.join(DIRECTORY_REPORTS, report_file), "r") as f:
            msg = email.message_from_bytes(bytearray(f.read(), "utf-8"))
            if send_email(msg):
                set_already_processed(project, build)
            else:
                print("Was not able to send email for report: {0}".format(report_file))

    return 0


def dump_object(obj, margin="", escape_nls=True):
    functions = []
    for var_name in dir(obj):
        if var_name.startswith("_"):
            continue

        value = getattr(obj, var_name)
        if type(value).__name__ == "method" or type(value).__name__ == "function":
            functions.append(var_name)
            continue

        if type(value) == str and escape_nls:
            value = value.replace("\r", "").replace("\n", "↵")

        print(
            "{0} - '{1}': [{2}] '{3}'".format(
                margin, var_name, type(value).__name__, value
            )
        )
    if functions:
        print("{0} - functions: {1}".format(margin, ", ".join(functions)))


def compare_builds(from_build, to_build, suites=None, environments=None):
    if suites == None:
        suites = ()

    if environments == None:
        environments = ()

    comparison = Project.compare_builds(from_build.id, to_build.id)

    if comparison["fixes"] and (suites or environments):
        filtered_fixes = {}
        for env in comparison["fixes"]:
            if environments and env not in environments:
                continue
            env_suites = comparison["fixes"][env]
            for suite in env_suites:
                if suites and suite not in suites:
                    continue
                if env not in filtered_fixes:
                    filtered_fixes[env] = {}
                filtered_fixes[env][suite] = env_suites[suite]
        comparison["fixes"] = filtered_fixes

    if comparison["regressions"] and (suites or environments):
        filtered_regressions = {}
        for env in comparison["regressions"]:
            if environments and env not in environments:
                continue
            env_suites = comparison["regressions"][env]
            for suite in env_suites:
                if suites and suite not in suites:
                    continue
                if env not in filtered_regressions:
                    filtered_regressions[env] = {}
                filtered_regressions[env][suite] = env_suites[suite]
        comparison["regressions"] = filtered_regressions

    return comparison


def debug(squad, args):
    success, jobs = parse_job_definitions(squad, args)

    print("Jobs:")
    pprint.PrettyPrinter(indent=2).pprint(jobs)


g_stages = {
    "scan": scan_builds,
    "generate": generate_reports,
    "notify": send_notifications,
    "send": send_reports,
    "list": list_builds,
    "slist": list_suites,
    "elist": list_environments,
    "debug": debug,
}


g_group_project_index = {}


def get_project(squad, group, project_slug):
    global g_group_project_index

    project_index = g_group_project_index.get(group.slug)
    if not project_index:
        group_projects = group.projects(count=-1)
        project_index = {project.slug: project for project in group_projects.values()}
        g_group_project_index[group.slug] = project_index

    return project_index.get(project_slug)


# Variants of build definitions
#
# Legend:
# '  ' - not provided, e.g. 'aa:bb:cc,' - no triplet for the second position
# '**' - any value or missing
# '++' - provided
# '--' - skipped, e.g. in 'aa:bb:' third triplet is skipped
#
#           triplets
#  first     second    third
# gr pr bl  gr pr bl  gr pr bl  description
# ========  ========  ========  =============================
#           ********  ********  error: missing group and project
# ++ -- --  ********  ********  error: missing project
# ++ ++ --                      report the most recent build, no comparison
# ++ ++ --        --        --  same, but compare to the previous and base builds
# ++ ++ --     ++ --        --  same, but compare to another project's last and base builds
# ++ ++ --  ++ ++ --        --  same, but compare to another group and project's last and base builds
# ++ ++ ++  ++ ++ ++  ++ ++ ++  explicit comparison of three fully qualified builds
#
# Examples:
#  "lkft" -> {'group': 'lkft'}
#  "lkft:linux-stable-rc-linux-5.10.y" -> {'group': 'lkft', 'project': 'linux-stable-rc-linux-5.10.y'}
#  "lkft:linux-stable-rc-linux-5.10.y:" -> {'group': 'lkft', 'project': 'linux-stable-rc-linux-5.10.y', 'build': '?'}


def parse_triplet(
    squad, triplet, is_first_triplet, require_project=True, require_build=False
):
    """Parse a triplet for the report job definition

    Parameters:
        squad           - Squad API object
        triplet         - a string containing the triplet
        is_first        - boolean, is ths a first triplet in the job definition

    Result:
        if mandatory parts are present, the following dictionary is returned:
            { 'group': <value>, 'project': <value>, 'build': <value }
        otherwise the following dictionary is returned
            { 'errors': [<error message>,...] }
    """
    needs_group = False
    needs_project = False
    needs_build = False
    group_slug = None
    group = None
    project_slug = None
    project = None
    build_version = None
    build = None
    errors = []

    parts = [part.strip() for part in triplet.split(":")]

    if len(parts) == 1:
        if parts[0] != "":
            if is_first_triplet:
                group_slug = parts[0]
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group_slug = None
                    group = None
                errors.append("Missing project")

            else:  # not first
                build_version = parts[0]
                if not build_version:
                    build_version = None
                    build = None
                    if require_build:
                        errors.append("Missing build")
                    else:
                        needs_build = True
                else:
                    build = project.build(build_version)
                    if not build:
                        errors.append("Unknown build '{0}'".format(build_version))
                        build_version = None
                        build = None

    elif len(parts) == 2:
        if is_first_triplet:
            group_slug = parts[0]
            if not group_slug:
                group = None
                group_slug = None
                errors.append("Missing group")
            else:
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group = None
                    group_slug = None

            project_slug = parts[1]
            if not project_slug:
                project_slug = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, group, project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

        else:  # not first
            project_slug = parts[0]
            if not project_slug:
                project_slug = None
                project = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, group, project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

            build_version = parts[1]
            if not build_version:
                build_version = None
                build = None
                if require_build:
                    errors.append("Missing build")
                else:
                    needs_build = True
            else:
                build = project.build(build_version)
                if not build:
                    errors.append("Unknown build '{0}'".format(build_version))
                    build_version = None
                    build = None

    else:  # >2
        if is_first_triplet:
            group_slug = parts[0]
            if not group_slug:
                group = None
                group_slug = None
                errors.append("Missing group")
            else:
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group = None
                    group_slug = None

            project_slug = parts[1]
            if not project_slug:
                project_slug = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, group, project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

            build_version = parts[2]
            if not build_version:
                build_version = None
                build = None
                if require_build:
                    errors.append("Missing build")
                else:
                    needs_build = True
            else:
                build = project.build(build_version)
                if not build:
                    errors.append("Unknown build '{0}'".format(build_version))
                    build_version = None
                    build = None

        else:  # not first
            group_slug = parts[0]
            if not group_slug:
                needs_group = True
                group_slug = None
                group = None
            else:
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group_slug = None
                    group = None

            project_slug = parts[1]
            if not project_slug:
                project_slug = None
                project = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, group, project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

            build_version = parts[2]
            if not build_version:
                build_version = None
                build = None
                if require_build:
                    errors.append("Missing build")
                else:
                    needs_build = True
            else:
                build = project.build(build_version)
                if not build:
                    errors.append("Unknown build '{0}'".format(build_version))
                    build_version = None
                    build = None

    result = {}

    if errors:
        result["errors"] = errors
    else:
        if needs_group:
            result["group"] = "?"
        elif group:
            result["group"] = group

        if needs_project:
            result["project"] = "?"
        elif project:
            result["project"] = project

        if needs_build:
            result["build"] = "?"
        elif build:
            result["build"] = build

    return result


def parse_job_definitions(squad, args, allow_incomplete=False):
    jobs = []
    success = True
    sources = (
        ("args", args.jobs),
        (
            ENV_JOBS,
            [item.strip() for item in os.environ.get(ENV_JOBS).split(";")]
                if os.environ.get(ENV_JOBS)
                else None,
        ),
    )

    for source_name, job_defs in sources:
        if not job_defs:
            continue

        if len(job_defs) == 1 and job_defs[0] == "":
            continue

        for job_def in job_defs:
            if not job_def:
                continue

            fq_triplets = []

            triplets = [item.strip() for item in job_def.split(",")]
            if len(triplets) == 1 and triplets[0] == "":
                continue

            previous_triplet = None

            for triplet in triplets:
                parsed_triplet = parse_triplet(
                    squad,
                    triplet,
                    not fq_triplets,
                    not allow_incomplete,
                    False
                )

                errors = parsed_triplet.get("errors")
                if errors:
                    success = False
                    print(
                        "Skipping job definition '{0}' due to errors:\n  {1}".format(
                            job_def, "\n  ".join(errors)
                        )
                    )

                else:
                    if (
                        not parsed_triplet.get("group")
                        and previous_triplet
                        and "group" in previous_triplet
                    ):
                        parsed_triplet["group"] = previous_triplet["group"]
                    if (
                        not parsed_triplet.get("project")
                        and previous_triplet
                        and "project" in previous_triplet
                    ):
                        parsed_triplet["project"] = previous_triplet["project"]
                    if not parsed_triplet.get("build"):
                        # TODO: look it up
                        if previous_triplet and "build" in previous_triplet:
                            parsed_triplet["build"] = previous_triplet["build"]

                    fq_triplets.append(parsed_triplet)

                previous_triplet = parsed_triplet

            if fq_triplets:
                job_definition = {"build": fq_triplets[0]}
                if len(fq_triplets) > 1:
                    job_definition["previous"] = fq_triplets[1]
                if len(fq_triplets) > 2:
                    job_definition["base"] = fq_triplets[2]
                jobs.append(job_definition)

        if jobs:
            break  # don't use env var if cmdline arg is provided

    return (success, jobs)


def main():
    global g_stages

    ap = argparse.ArgumentParser(description="LKFT stable report")
    ap.add_argument(
        "-j",
        "--job",
        dest="jobs",
        action="append",
        type=str,
        help="jobs to process",
    )
    ap.add_argument(
        "-s",
        "--suite",
        dest="suites",
        action="append",
        default=[],
        type=str,
        help="test suites to process",
    )
    ap.add_argument(
        "-e",
        "--environment",
        dest="environments",
        action="append",
        default=[],
        type=str,
        help="test environments to process",
    )
    ap.add_argument(
        "-a",
        "--all",
        dest="all",
        action="store_true",
        default=False,
        help="process all builds, including the unfinished ones"
    )
    ap.add_argument(
        "stages",
        metavar="stage",
        type=str,
        nargs="+",
        choices=g_stages.keys(),
        help="mention which stage(s) to run, one or more of: {0}".format(
            ", ".join(g_stages.keys())
        ),
    )

    args = ap.parse_args()

    if len(args.stages):
        SquadApi.configure(url="https://qa-reports.linaro.org/")
        squad = Squad()
        for stage in args.stages:
            function = g_stages.get(stage)
            if not function:
                print("Unknown stage: {0}, skipped".format(stage))
                continue

            result = function(squad, args)
            if result:
                return result

    return 0


if __name__ == "__main__":
    sys.exit(main())
